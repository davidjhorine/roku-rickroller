// Allow discalimer to be accepted and dismissed
function acceptDisclaimer(){
    let disclaimer = document.getElementsByClassName('disclaimerPopup')[0];
    disclaimer.classList.add('disclaimerAccepted')
    // Wait for the animation to finish before removing the element
    setTimeout(() => {
        disclaimer.style.display = 'none';
    }, 200)
}

// Get value of radio button group
function getRadioValue(radioGroup) {
    let radioButtons = radioGroup.querySelectorAll('input[type=radio]');
    let selected = '';
    radioButtons.forEach(button => {
        if (button.checked) {
            selected = button.value;
        }
    });
    return selected;
}

// Change the text on the main action button depending on which search mode is selected
function updateSearchMode() {
    let searchModeGroup = document.getElementsByClassName('searchTypeRadio')[0];
    let searchMode = getRadioValue(searchModeGroup);
    let goButton = document.getElementsByClassName('goButton')[0];
    if (searchMode == "manual") {
        goButton.innerHTML = "Send the rickroll!"
    } else {
        goButton.innerHTML = "Search for Roku devices"
    }
}

// Check to see if a specified IP address is a roku
function checkIp(ipAddr) {
    let ipTestZone = document.getElementById('ipTestZone');
    // Scripts are not subject to CORS limitations
    let newScript = document.createElement('script');
    // Roku devices will return an XML tree that won't load as a script,
    // but can confirm the existance of a Roku device
    newScript.src = "http://" + ipAddr + ":8060/query/apps";
    newScript.dataset.ipAddr = ipAddr;
    newScript.onload = () => {
        confirmIpAddr(ipAddr);
        progressUpdate();
        newScript.remove();
    };
    newScript.onerror = () => {
        progressUpdate();
    };
    // Normally, these scripts take a long time to error.
    // This assumes that if the script hasn't loaded in 100 milliseconds, it won't load.
    setTimeout(() => {
        newScript.remove();
    }, 100) 
    ipTestZone.append(newScript);
}

// Handle confirmed IP Addresses
function confirmIpAddr(ipAddr) {
    // Clone and append the foundRoku template
    let searchResults = document.getElementsByClassName('rokuSearchResults')[0];
    let rokuFoundTemplate = document.getElementById('rokuFoundTemplate');
    let newFoundRoku = rokuFoundTemplate.content.cloneNode(true);
    searchResults.append(newFoundRoku);
    // Change the information for the template
    let appRokuFound = searchResults.lastElementChild;
    appRokuFound.dataset.targetIp = ipAddr;
    appRokuFound.querySelector('div').innerHTML = ipAddr;
}

// This function will iterate through every address 192.168.0.0 through 192.168.maxP1Value.255,
// making sure only 1 IP is being checked at a time
function findRokuDevices(maxP1Value, currentP1Value, currentP2Value) {
    let testZone = document.getElementById('ipTestZone');
    // If the scanning isn't done
    if (currentP1Value <= maxP1Value) {
        // If there's not already a scan going
        if (testZone.children.length == 0) {
            checkIp("192.168." + currentP1Value + "." + currentP2Value);
            // Increment p2 if less than 255, else reset p2 and increment p1
            if (currentP2Value < 255) {
                currentP2Value++;
            } else {
                currentP2Value = 0;
                currentP1Value++;
            }
            // Check the next IP in 100 milliseconds
            setTimeout(() => {
                findRokuDevices(maxP1Value, currentP1Value, currentP2Value);
            }, 100)
        } else {
            // Try to scan again in 100 milliseconds if there is already a scan going
            setTimeout(() => {
                findRokuDevices(maxP1Value, currentP1Value, currentP2Value);
            }, 100)
        }
    }
}

// Update the value of the search progress
function progressUpdate() {
    let progressBar = document.getElementById('searchProgressBar');
    let progressText  = document.getElementById('searchProgressText');
    let currentProgress = parseInt(progressText.dataset.current) + 1;
    let maxProgress = parseInt(progressText.dataset.max);
    progressText.dataset.current = currentProgress;
    progressText.innerHTML = currentProgress + " of " + maxProgress + " scanned";
    progressBar.setAttribute('value', currentProgress);
}

// Reset the search progress
function progressReset(max) {
    // The search popup will need to be shown when progressReset is called
    let searchBox = document.getElementsByClassName('searchPopup')[0];
    searchBox.style.display = 'flex';
    let progressBar = document.getElementById('searchProgressBar');
    let progressText  = document.getElementById('searchProgressText');
    progressText.dataset.max = max;
    progressText.dataset.current = 0;
    progressText.innerHTML = 0 + " of " + max + " scanned";
    progressBar.setAttribute('value', 0);
    progressBar.setAttribute('max', max);
}

// Send a Rickroll to a Roku device
function sendRickroll(ipAddr) {
    let videoSelector = document.getElementsByClassName('videoRadio')[0];
    let videoName = getRadioValue(videoSelector);
    const videoList = {
        'neverGonnaGive' : 'dQw4w9WgXcQ',
        'computerMan' : 'jeg_TJvkSjg',
        'nyanCat' : 'QH2-TGUlwu4'
    }
    let url = 'http://' + ipAddr + ':8060/launch/837?contentId=' + videoList[videoName] + '&mediaType=shortFormVideo';
    // Fetch will always error out because of CORS, causing any code after it to not get executed.
    // Waiting for an error will result in a delay between the user clicking the button and a 
    // confirmation. Using setTimeout will cause the alert to fire instantly without blocking or
    // being blocked by fetch.
    setTimeout(()=>{
        alert('Attempted to send a Rickroll to ' + ipAddr)
    }, 0)
    fetch(url,{'method':'POST'})
}

// Figure out what to do when the user clicks the go button
function goButton(){
    let searchTypeRadio = document.getElementsByClassName('searchTypeRadio')[0];
    let searchType = getRadioValue(searchTypeRadio);
    if (searchType == 'shortSearch') {
        progressReset(2816);
        findRokuDevices(10,0,0);
    } else if (searchType == 'longSearch') {
        progressReset(65536);
        findRokuDevices(255,0,0);
    } else if (searchType == 'manual') {
        let manualIpInput = document.getElementById('manualIp');
        sendRickroll(manualIpInput.value);
    }
}

// Toggle a "dark mode" with text and background colors inverted
function toggleDarkMode(){
    let primaryOne = getComputedStyle(document.body).getPropertyValue('--primaryColorOne');
    if (primaryOne.includes("#BBCDE5")) {
        document.body.style.setProperty('--primaryColorOne',' #222');
        document.body.style.setProperty('--primaryColorTwo',' #BBCDE5');
    } else {
        document.body.style.setProperty('--primaryColorTwo',' #222');
        document.body.style.setProperty('--primaryColorOne',' #BBCDE5');
    }
}

// Add alert if the user tries to access the website
// over HTTPS. Most browsers won't allow access to the
// insecure Roku API from a secure website
function checkPageProtocol(){
    if (location.protocol == "https:") {
        let protocol = document.getElementsByClassName('protocolPopup')[0];
        protocol.style.display = 'flex';
    }
}

// Allow the user to bypass the HTTP redirect
function acceptProtocol(){
    let protocol = document.getElementsByClassName('protocolPopup')[0];
    protocol.classList.add('protocolAccepted')
    // Wait for the animation to finish before removing the element
    setTimeout(() => {
        protocol.style.display = 'none';
    }, 200)
}

function setup() {
    // Check for an HTTPS connection
    checkPageProtocol()
    // Check if the user's system is set to light or dark mode
    let darkPreference = window.matchMedia('(prefers-color-scheme: dark)').matches;
    if (darkPreference) {
        toggleDarkMode()
    }
    // Fix the go button's text after a page reload
    updateSearchMode();
}

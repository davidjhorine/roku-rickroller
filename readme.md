# Roku Rickroller
### Search and rickroll Roku devices on your network
## Disclaimer
### Hey! Before you use this!
Please only use this on devices you own or are authorized to use it on. The website creator, the website, and Gitlab are in no way responsible for any consequences that arise from unauthorized use of this website.

## How to use it
1. Go to [this website](http://roku-rickroller.davidjhorine.com/)
2. Select your search mode
3. Select from one of the three videos (Never Gonna Give You Up, Computer Man, and Nyan Cat)
4. Click the button that says "Search for Roku devices" or "Send the rickroll!"
5. If you searched for Roku devices, wait for them to pop up, then click "Send the rickroll!"

## How does this work and how bad is it?
### Searching for Roku devices
Your browser has a security feature called CORS to prevent websites from getting data from outside sources unless the source has said that website can get data. While all Javascript requests respect CORS, the <\script> tag does not.

Roku Rickroller creates a new script element with the source set to `http://<ip address getting scanned>:8060/query/apps`, which is a URL present on all Roku devices. While the page's Javascript can't read the response, it can register a listener for the onload and onerror events to determine if the URL exists. By checking if the URL exists, the page can tell which IP's are Roku devices.

### Controlling Roku devices
Roku devices expose an HTTP API on port 8060 (documentation [here](https://developer.roku.com/docs/developer-program/debugging/external-control-api.md#!)) that allows for applications to control Roku devices as if they were a Roku remote. Unlike the physical Roku remotes, this API has no pairing or authentication to make sure the user wants a specific application controlling their Roku. The `fetch` function in Javascript can be used to make a POST request to the API, and Roku devices will respond to it as if it were a Roku remote. 

Roku Rickroller uses the deep linking feature of the Roku ECP API, which allows for an application to launch a channel into specific content. Roku Rickroller makes a POST request to `/launch/837?contentId=<video id>`, which launches the YouTube app to a specific video.

### How bad is this?
What a website could do:
 - Most things your Roku remote can do
   - Restart your Roku
   - Change your Roku settings
   - Factory reset your Roku
 - Install and launch any app on the Roku store
 - Launch into specific content within an app
 - Open a system search for anything
 - Communicate data to an open app

What a website can't do:
 - Get information back from the API (blocked by CORS)
 - Enable developer mode or change your developer mode password (only works from a physical remote)
 - Execute arbitrary code (developer mode can't be enabled)

### Does Roku know about this?
Yes! They even have a [website](http://devtools.web.roku.com/RokuRemote/) that uses this to script remote inputs for developers.